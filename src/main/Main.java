package main;

import java.awt.*;
import java.awt.event.*;
import java.awt.image.*;
import java.util.LinkedList;
import java.io.File;
import java.io.IOException;

import javax.swing.*;
import javax.imageio.ImageIO;

import main.Hitbox;
import main.Character;

public class Main extends JPanel implements ActionListener {

    /**
     * 
     */
    private static final long serialVersionUID = -4049926118389340414L;

    private static JFrame frame;
    private static JMenuBar menubar;
    private static Timer tim;
    private static LinkedList<Hitbox> hitboxen;
    private static Hitbox hb;
    private static Character ch;
    private static Image image;
    private static int unity;

    public Main() {
        int x, y;
        setSize(800, 640);
        x = this.getWidth();
        y = this.getHeight();
        
        image = new ImageIcon(Main.class.getResource("data/TileD.png")).getImage();
        unity = 5;

        this.addKeyListener(new KeyboardIn());
        setFocusable(true);

        menubar = new JMenuBar();
        JMenu file = new JMenu("File");
        tim = new Timer(20, this);
        tim.start(); 

        hitboxen = new LinkedList<Hitbox>();
        /*
         * a sketch for a character hitbox list:
         *       __
         *      |oo|  Yo, dawg, I'm a headbox.
         *     _|~~|_  Cool moustache.
         *    /|    |\ Yo, I'm several armboxen and a torsobox.
         *   / |    | \
         *  /  |    |  \
         * o   |    |   o Yo, I'm two handboxen.
         *     |____|
         *     /_/\_\    Yo, I ain't got a groin, but I got several legboxen.
         *    /_/  \_\
         *   /_/    \_\
         *  /_/      \_\ 
         * |___\    /___| Yo, I got two footboxen! I'm not Balrog! What's a kick?
         * 
         * 
         */

        int[] xarr = {50, 100, 100, 50 };
        int[] yarr = {y-80,  y-80, y-150, y-150};
        hb = new Hitbox(4, xarr.clone(), yarr.clone()); // torso
        hitboxen.add(hb);
        xarr[0] = 50; xarr[1] = 40; xarr[2] = 55; xarr[3] = 65;
        yarr[0] = y-80; yarr[1] = y-50; yarr[2] = y-50; yarr[3] = y-80; 
        hb = new Hitbox(4, xarr.clone(), yarr.clone()); // rlegt
        hitboxen.add(hb);
        xarr[0] = 85; xarr[1] = 95; xarr[2] = 110; xarr[3] = 100; 
        hb = new Hitbox(4, xarr.clone(), yarr.clone()); // llegt
        hitboxen.add(hb);

        ch = new Character(hitboxen, 0, 0);


        /*
         * begin file menu items
         */
        JMenuItem fileClose = new JMenuItem("Close", null);
        fileClose.setToolTipText("Exit application");
        fileClose.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                System.exit(0);
            }

        });
        file.add(fileClose);

        JMenuItem restart = new JMenuItem("Restart", null);
        restart.setMnemonic(KeyEvent.VK_F10);
        restart.setToolTipText("restart game");
        restart.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                // do restart
            }
        });
        file.add(restart);


        menubar.add(file);
        /*
         * end file menu items
         */



        /*
         * begin input menu items
         */

        JMenu input = new JMenu("Input");
        input.setMnemonic(KeyEvent.VK_I);

        JMenuItem kb = new JMenuItem("Keyboard", null);
        kb.setToolTipText("configure keyboard layout");
        kb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                // show keyboard config
            }
        });
        input.add(kb);

        menubar.add(input);

        /*
         * end input menu items
         */


        /*
         * begin view menu items
         */

        JMenu view = new JMenu("View");

        JCheckBoxMenuItem showfps = new JCheckBoxMenuItem("show FPS");
        showfps.setState(true);
        showfps.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                // show fps or not
            }

        });

        view.add(showfps);

        JCheckBoxMenuItem showhb = new JCheckBoxMenuItem("show hitboxen");
        showhb.setState(true);
        showhb.addActionListener(new ActionListener() {
            public void actionPerformed(ActionEvent event) {
                // show fps or not
            }

        });

        view.add(showhb);

        menubar.add(view);

        /*
         * end view menu items
         */


    }

    public static void main(String[] args) {
        Main game = new Main();
        frame = new JFrame("Game");
        frame.add(game);
        frame.setJMenuBar(menubar);
        frame.setSize(800, 640);
        frame.setTitle("A Game");
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.setLocationRelativeTo(null);
        frame.setVisible(true);
    }

    public void paintComponent(Graphics g) {
        g.setColor(Color.RED);
        super.paintComponent(g);
        //g.drawImage(image, 50, 50, this);
        ch.paint(g);
    }

    @Override
    public void actionPerformed(ActionEvent arg0) {
        repaint();

    }

    class KeyboardIn extends KeyAdapter {
        public void keyPressed(KeyEvent e) {

            int keyc = e.getKeyCode();

            if (keyc == KeyEvent.VK_RIGHT) {
                ch.move(unity, 0);
            } else if (keyc == KeyEvent.VK_LEFT) {
                ch.move(-unity, 0);
            } else if (keyc == KeyEvent.VK_UP) {
                ch.move(0, -unity);
            } else if (keyc == KeyEvent.VK_DOWN) {
                ch.move(0, unity);
            }


        }

    }

}
