package main;

import java.util.LinkedList;
import java.awt.Graphics;
import java.awt.Color;

import main.Hitbox;

public class Character {

    protected LinkedList<Hitbox> hitboxen;
    protected int x, y;

    // other stuff
    
    // constructor begin
    public Character(LinkedList<Hitbox> hitboxen, int x, int y) {
        this.hitboxen = hitboxen;
        this.x = x;
        this.y = y;
    }

    // getters, setters
    public LinkedList<Hitbox> getHitboxen() {
        return hitboxen;
    }
    public void setHitboxen(LinkedList<Hitbox> hitboxen) {
        this.hitboxen = hitboxen;
    }
    public int getX() {
        return x;
    }
    public void setX(int x) {
        this.x = x;
    }
    public int getY() {
        return y;
    }
    public void setY(int y) {
        this.y = y;
    }

    // misc methods
    public void paint(Graphics g) {
        g.setColor(Color.RED);
        for(int i=0; i<hitboxen.size(); i++) {
            hitboxen.get(i).paint(g);
        }
    }

    public void move(int xd, int yd) {
        for(int i=0; i<hitboxen.size(); i++) {
            hitboxen.get(i).move(xd, yd);
        }
    }



}
