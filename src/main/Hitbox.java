package main;

import java.awt.Graphics;

public class Hitbox {
    protected int n;
    protected int[] xs;
    protected int[] ys;
    
    // constructor begin
    public Hitbox(int n, int[] xs, int[] ys) {
        this.n = n;
        this.xs = xs;
        this.ys = ys;
    }
    
    // getters, setters
    public int getN() {
        return n;
    }
    public void setN(int n) {
        this.n = n;
    }
    public int[] getXcoords() {
        return xs;
    }
    public void setXs(int[] xs) {
        this.xs = xs;
    }
    public int[] getYs() {
        return ys;
    }
    public void setYs(int[] ys) {
        this.ys = ys;
    }
    
    
    
    // misc methods
    public void paint(Graphics g) {
        g.drawPolygon(xs, ys, n);
    }
    
    public void move(int xd, int yd) {
        for(int i=0; i<n; i++) {
            xs[i] += xd;
            ys[i] += yd;
        }
    }
    

}
